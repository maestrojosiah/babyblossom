<?php

namespace App\Form;

use App\Entity\Configuration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('companyName')
            ->add('siteEmail')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('country')
            ->add('telephone')
            ->add('facebookId')
            ->add('TwitterId')
            ->add('websiteTheme', ChoiceType::class, [
                'choices' => [
                    'Site' => 'site',
                ]
            ])
            ->add('companyLogo', FileType::class, [
                'label' => 'Attachment: PDF, word, excel, powerpoint, csv, zip, png and jpeg',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Files allowed: png and jpeg.',
                    ])
                ],
            ])            
            ->add('invoiceLogo', FileType::class, [
                'label' => 'Attachment: PDF, word, excel, powerpoint, csv, zip, png and jpeg',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Files allowed: png and jpeg.',
                    ])
                ],
            ])            
            ->add('shortDate', ChoiceType::class, [
                'choices' => [
                    'YYYY-MM-DD' => 'Y-m-d',
                    'YY-MM-DD' => 'y-m-d',
                    'YY-M-D' => 'y-m-d',
                ]
            ])
            ->add('timeFormat')
            ->add('timeFormat', ChoiceType::class, [
                'choices' => [
                    '02:00pm' => 'h:ia',
                    '14:00' => 'H:i',
                ]
            ])
            ->add('longDate')
            ->add('longDate', ChoiceType::class, [
                'choices' => [
                    '5th October 2022' => 'jS M Y',
                    'October 5 2022' => 'M j Y',
                ]
            ])
            ->add('defTimeZone', ChoiceType::class, [
                'choices' => [
                    'Africa/Nairobi' => 'Africa/Nairobi',
                ]
            ])
            ->add('defLocale', ChoiceType::class, [
                'choices' => [
                    'English (US)' => 'en-us',
                ]
            ])
            ->add('weekStartsOn', ChoiceType::class, [
                'choices' => [
                    'Sunday' => 'sunday',
                    'Monday' => 'monday',
                    'Tuesday' => 'tuesday',
                    'Wednesday' => 'wednesday',
                    'Thursday' => 'thursday',
                    'Friday' => 'friday',
                    'Saturday' => 'saturday',
                ]
            ])
            ->add('invoiceHeader', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
            ->add('invoiceFooter', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
            
            ->add('siteOffLine')
            ->add('itemsPerPage', ChoiceType::class, [
                'choices' => [
                    '25' => '25',
                    '50' => '50',
                    '100' => '100',
                    '150' => '150',
                ]
            ])
            ->add('invoicePageFormat', ChoiceType::class, [
                'choices' => [
                    'Letter' => 'letter',
                    'A4' => 'A4',
                ]
            ])
            ->add('defaultCurrency')
            ->add('showSlider')
            ->add('siteMetaKeywords')
            ->add('siteDescription')
            ->add('newsLength', ChoiceType::class, [
                'choices' => [
                    'One (1)' => '1',
                    'Two (2)' => '2',
                    'Three (3)' => '3',
                    'Four (4)' => '4',
                ]
            ])
            ->add('telephoneAlt')
            ->add('emailAlt')
            ->add('instagramId')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Configuration::class,
        ]);
    }
    
}
