<?php

namespace App\Entity;

use App\Repository\ConfigurationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConfigurationRepository::class)]
class Configuration
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $companyName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $siteEmail = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $state = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $country = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $telephone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $facebookId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $TwitterId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $websiteTheme = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $companyLogo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $shortDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $timeFormat = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $longDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $defTimeZone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $defLocale = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $weekStartsOn = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $invoiceHeader = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $invoiceFooter = null;

    #[ORM\Column(nullable: true)]
    private ?bool $siteOffLine = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $itemsPerPage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $invoicePageFormat = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $defaultCurrency = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $showSlider = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $siteMetaKeywords = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $siteDescription = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $newsLength = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $telephoneAlt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $emailAlt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $instagramId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $invoiceLogo = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getSiteEmail(): ?string
    {
        return $this->siteEmail;
    }

    public function setSiteEmail(?string $siteEmail): self
    {
        $this->siteEmail = $siteEmail;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getTwitterId(): ?string
    {
        return $this->TwitterId;
    }

    public function setTwitterId(?string $TwitterId): self
    {
        $this->TwitterId = $TwitterId;

        return $this;
    }

    public function getWebsiteTheme(): ?string
    {
        return $this->websiteTheme;
    }

    public function setWebsiteTheme(?string $websiteTheme): self
    {
        $this->websiteTheme = $websiteTheme;

        return $this;
    }

    public function getCompanyLogo(): ?string
    {
        return $this->companyLogo;
    }

    public function setCompanyLogo(?string $companyLogo): self
    {
        $this->companyLogo = $companyLogo;

        return $this;
    }

    public function getShortDate(): ?string
    {
        return $this->shortDate;
    }

    public function setShortDate(?string $shortDate): self
    {
        $this->shortDate = $shortDate;

        return $this;
    }

    public function getTimeFormat(): ?string
    {
        return $this->timeFormat;
    }

    public function setTimeFormat(?string $timeFormat): self
    {
        $this->timeFormat = $timeFormat;

        return $this;
    }

    public function getLongDate(): ?string
    {
        return $this->longDate;
    }

    public function setLongDate(?string $longDate): self
    {
        $this->longDate = $longDate;

        return $this;
    }

    public function getDefTimeZone(): ?string
    {
        return $this->defTimeZone;
    }

    public function setDefTimeZone(?string $defTimeZone): self
    {
        $this->defTimeZone = $defTimeZone;

        return $this;
    }

    public function getDefLocale(): ?string
    {
        return $this->defLocale;
    }

    public function setDefLocale(?string $defLocale): self
    {
        $this->defLocale = $defLocale;

        return $this;
    }

    public function getWeekStartsOn(): ?string
    {
        return $this->weekStartsOn;
    }

    public function setWeekStartsOn(?string $weekStartsOn): self
    {
        $this->weekStartsOn = $weekStartsOn;

        return $this;
    }

    public function getInvoiceHeader(): ?string
    {
        return $this->invoiceHeader;
    }

    public function setInvoiceHeader(?string $invoiceHeader): self
    {
        $this->invoiceHeader = $invoiceHeader;

        return $this;
    }

    public function getInvoiceFooter(): ?string
    {
        return $this->invoiceFooter;
    }

    public function setInvoiceFooter(?string $invoiceFooter): self
    {
        $this->invoiceFooter = $invoiceFooter;

        return $this;
    }

    public function isSiteOffLine(): ?bool
    {
        return $this->siteOffLine;
    }

    public function setSiteOffLine(?bool $siteOffLine): self
    {
        $this->siteOffLine = $siteOffLine;

        return $this;
    }

    public function getItemsPerPage(): ?string
    {
        return $this->itemsPerPage;
    }

    public function setItemsPerPage(?string $itemsPerPage): self
    {
        $this->itemsPerPage = $itemsPerPage;

        return $this;
    }

    public function getInvoicePageFormat(): ?string
    {
        return $this->invoicePageFormat;
    }

    public function setInvoicePageFormat(?string $invoicePageFormat): self
    {
        $this->invoicePageFormat = $invoicePageFormat;

        return $this;
    }

    public function getDefaultCurrency(): ?string
    {
        return $this->defaultCurrency;
    }

    public function setDefaultCurrency(?string $defaultCurrency): self
    {
        $this->defaultCurrency = $defaultCurrency;

        return $this;
    }

    public function getShowSlider(): ?string
    {
        return $this->showSlider;
    }

    public function setShowSlider(?string $showSlider): self
    {
        $this->showSlider = $showSlider;

        return $this;
    }

    public function getSiteMetaKeywords(): ?string
    {
        return $this->siteMetaKeywords;
    }

    public function setSiteMetaKeywords(?string $siteMetaKeywords): self
    {
        $this->siteMetaKeywords = $siteMetaKeywords;

        return $this;
    }

    public function getSiteDescription(): ?string
    {
        return $this->siteDescription;
    }

    public function setSiteDescription(?string $siteDescription): self
    {
        $this->siteDescription = $siteDescription;

        return $this;
    }

    public function getNewsLength(): ?string
    {
        return $this->newsLength;
    }

    public function setNewsLength(?string $newsLength): self
    {
        $this->newsLength = $newsLength;

        return $this;
    }

    public function getTelephoneAlt(): ?string
    {
        return $this->telephoneAlt;
    }

    public function setTelephoneAlt(?string $telephoneAlt): self
    {
        $this->telephoneAlt = $telephoneAlt;

        return $this;
    }

    public function getEmailAlt(): ?string
    {
        return $this->emailAlt;
    }

    public function setEmailAlt(?string $emailAlt): self
    {
        $this->emailAlt = $emailAlt;

        return $this;
    }

    public function getInstagramId(): ?string
    {
        return $this->instagramId;
    }

    public function setInstagramId(?string $instagramId): self
    {
        $this->instagramId = $instagramId;

        return $this;
    }

    public function getInvoiceLogo(): ?string
    {
        return $this->invoiceLogo;
    }

    public function setInvoiceLogo(?string $invoiceLogo): self
    {
        $this->invoiceLogo = $invoiceLogo;

        return $this;
    }
}
