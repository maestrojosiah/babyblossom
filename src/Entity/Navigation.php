<?php

namespace App\Entity;

use App\Repository\NavigationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: NavigationRepository::class)]
class Navigation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $link = null;

    #[ORM\Column]
    private ?bool $hasChildren = null;

    #[ORM\Column(length: 255)]
    private ?string $position = null;

    #[ORM\OneToMany(mappedBy: 'navigation', targetEntity: NavChild::class, orphanRemoval: true)]
    private Collection $navChildren;

    public function __construct()
    {
        $this->navChildren = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function isHasChildren(): ?bool
    {
        return $this->hasChildren;
    }

    public function setHasChildren(bool $hasChildren): self
    {
        $this->hasChildren = $hasChildren;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection<int, NavChild>
     */
    public function getNavChildren(): Collection
    {
        return $this->navChildren;
    }

    public function addNavChild(NavChild $navChild): self
    {
        if (!$this->navChildren->contains($navChild)) {
            $this->navChildren->add($navChild);
            $navChild->setNavigation($this);
        }

        return $this;
    }

    public function removeNavChild(NavChild $navChild): self
    {
        if ($this->navChildren->removeElement($navChild)) {
            // set the owning side to null (unless already changed)
            if ($navChild->getNavigation() === $this) {
                $navChild->setNavigation(null);
            }
        }

        return $this;
    }
}
