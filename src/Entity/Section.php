<?php

namespace App\Entity;

use App\Repository\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SectionRepository::class)]
class Section
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $typography = null;

    #[ORM\Column]
    private ?int $position = null;

    #[ORM\Column(nullable: true)]
    private ?bool $visible = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $lastUpdated = null;

    #[ORM\ManyToOne(inversedBy: 'sections')]
    private ?Page $page = null;

    #[ORM\OneToMany(mappedBy: 'section', targetEntity: WebPhotos::class)]
    private Collection $webPhotos;

    public function __construct()
    {
        $this->webPhotos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTypography(): ?string
    {
        return $this->typography;
    }

    public function setTypography(string $typography): self
    {
        $this->typography = $typography;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function isVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getLastUpdated(): ?\DateTimeImmutable
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(\DateTimeImmutable $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return Collection<int, WebPhotos>
     */
    public function getWebPhotos(): Collection
    {
        return $this->webPhotos;
    }

    public function addWebPhoto(WebPhotos $webPhoto): self
    {
        if (!$this->webPhotos->contains($webPhoto)) {
            $this->webPhotos->add($webPhoto);
            $webPhoto->setSection($this);
        }

        return $this;
    }

    public function removeWebPhoto(WebPhotos $webPhoto): self
    {
        if ($this->webPhotos->removeElement($webPhoto)) {
            // set the owning side to null (unless already changed)
            if ($webPhoto->getSection() === $this) {
                $webPhoto->setSection(null);
            }
        }

        return $this;
    }
}
