<?php

namespace App\Controller;

use App\Entity\WebPhotos;
use App\Form\WebPhotosType;
use App\Repository\WebPhotosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/web/photos')]
class WebPhotosController extends AbstractController
{
    #[Route('/', name: 'app_web_photos_index', methods: ['GET'])]
    public function index(WebPhotosRepository $webPhotosRepository): Response
    {
        return $this->render('web_photos/index.html.twig', [
            'web_photos' => $webPhotosRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_web_photos_new', methods: ['GET', 'POST'])]
    public function new(Request $request, WebPhotosRepository $webPhotosRepository): Response
    {
        $webPhoto = new WebPhotos();
        $form = $this->createForm(WebPhotosType::class, $webPhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $webPhotosRepository->save($webPhoto, true);

            return $this->redirectToRoute('app_web_photos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('web_photos/new.html.twig', [
            'web_photo' => $webPhoto,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_web_photos_show', methods: ['GET'])]
    public function show(WebPhotos $webPhoto): Response
    {
        return $this->render('web_photos/show.html.twig', [
            'web_photo' => $webPhoto,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_web_photos_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, WebPhotos $webPhoto, WebPhotosRepository $webPhotosRepository): Response
    {
        $form = $this->createForm(WebPhotosType::class, $webPhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $webPhotosRepository->save($webPhoto, true);

            return $this->redirectToRoute('app_web_photos_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('web_photos/edit.html.twig', [
            'web_photo' => $webPhoto,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_web_photos_delete', methods: ['POST'])]
    public function delete(Request $request, WebPhotos $webPhoto, WebPhotosRepository $webPhotosRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$webPhoto->getId(), $request->request->get('_token'))) {
            $webPhotosRepository->remove($webPhoto, true);
        }

        return $this->redirectToRoute('app_web_photos_index', [], Response::HTTP_SEE_OTHER);
    }
}
