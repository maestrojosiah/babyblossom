<?php

namespace App\Controller;

use App\Repository\BlogCategoryRepository;
use App\Repository\BlogRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\NavigationRepository;
use App\Repository\PageRepository;
use App\Repository\SectionRepository;
use App\Repository\UserRepository;
use App\Repository\WebPhotosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing;
use Symfony\Bridge\Twig\Extension\RoutingExtension;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\UrlPackage;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\Packages;
use Symfony\Bridge\Twig\Extension\AssetExtension;

class DefaultController extends AbstractController
{

    private $usr;
    private $config;
    private $nav;
    private $blog;
    private $sectionRepo;
    private $photoRepo;
    private $pageRepo;
    private $blogCategoryRepo;

    public function __construct(
        UserRepository $usr, 
        ConfigurationRepository $config, 
        NavigationRepository $nav, 
        BlogRepository $blog,
        SectionRepository $sectionRepo,
        WebPhotosRepository $photoRepo,
        PageRepository $pageRepo,
        BlogCategoryRepository $blogCategoryRepo,
        )
    {
        $this->usr = $usr;
        $this->config = $config;
        $this->nav = $nav;
        $this->blog = $blog;
        $this->sectionRepo = $sectionRepo;
        $this->photoRepo = $photoRepo;
        $this->pageRepo = $pageRepo;
        $this->blogCategoryRepo = $blogCategoryRepo;
    }
    
    #[Route('/', name: 'app_default')]
    public function index(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {
        if(count($this->config->findAll()) == 0){
            return $this->redirectToRoute('app_configuration_new');
        }

        // this page
        $page = $pageRepo->findOneByName('homepage');
        // $teachers = $this->usr->findTeachersByRand();

        // html is now empty
        $html = "";
        $photosForThisContent = [];
        $organizedSections = $sectionRepo->findBy(
            array('page' => $page, 'visible' => 1),
            array('position' => 'ASC'),
            30
        );

        $html = $this->generateHtml($organizedSections);

        $glVars = $this->getGlobalVars(3,15);
        $pagePhotos = $this->getAllPagePhotos($page);
        // $glVars['teachers'] = $teachers;

        $rendered = $this->generateTemplate($html, 'index.html', ['vars' => $glVars, 'photos' => $pagePhotos]);

        return $this->render('default/index.html.twig', [
            'global_vars' => $glVars,
            'rendered' => html_entity_decode($rendered),
        ]);

    }

    #[Route('/logout/page', name: 'logout_page')]
    public function logout(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    {

        return $this->render('security/logout.html.twig', []);

    }

    function generateHtml($organizedSections){

        $html = "";

        // iterate sections to find content
        foreach ($organizedSections as $section ) {

            // div start
            $bhtml = "<div class='sec' id='section_" . $section->getId() . "'>";
            // $bhtml .= "<button type='button' style='z-index:1000;' id='saveChanges_".$section->getId()."' class='save-changes-btn button'>Save changes</button>";
            if ($this->isGranted('ROLE_ADMIN')) {
                $bhtml .= "<a href='/section/".$section->getId()."/edit' style='z-index:1001;' id='edit_".$section->getId()."' target='_blank' class='save-changes-btn nicdark_background_none_hover nicdark_color_white_hover nicdark_border_2_solid_white_hover nicdark_border_2_solid_green nicdark_transition_all_08_ease nicdark_display_inline_block nicdark_text_align_center nicdark_box_sizing_border_box nicdark_color_white nicdark_bg_green nicdark_first_font nicdark_padding_10_20 nicdark_border_radius_3'>Edit " . $section->getName() . "</a>";
            }

            // generate page forms for uploading images
            list($buildImgLinks, $photos) = $this->getImgForms($section->getWebPhotos());

            // main section text
            $bhtml .= $section->getTypography();

            // if is admin, then put in the forms for uploading images
            if ($this->isGranted('ROLE_ADMIN')) {
                $bhtml .= "<div class='attach-bottom'>" . $buildImgLinks . "</div>";
            }            
            
            // div end
            $bhtml .= "</div>";
            
            // clear any floats
            $bhtml .= "<div style='clear:both;float:none;'></div>";

            // clean the code and add in the real images
            $html .= htmlspecialchars_decode($bhtml);

        }

        return $html;

    }    
    function getGlobalVars($blogCount = 3, $courseCount = 3){

        $data = [];
        $configData = $this->config->findAll();
        $navData = $this->nav->findAll();
        $blogCategories = $this->blogCategoryRepo->findAll();
        $url = parse_url($_SERVER['REQUEST_URI']);

        $glVars = [];
        $glVars['configData'] = $configData ? $configData[0]: null;
        $glVars['navData'] = $navData;
        $glVars['thisUrl'] = $url['path'];
        $glVars['thisUser'] = $this->getUser();
        $glVars['blogCategories'] = $blogCategories;
        $glVars['blogs'] = $this->blog->findBy(
            array(),
            array('published_at' => 'DESC'),
            $blogCount
        );

        return $glVars;

    }

    function getAllPagePhotos($page){
        $photos = [];
        foreach ($page->getSections() as $key => $section) {
            $sectionImages = $this->photoRepo->findBy(
                ['section' => $section],
                ['id' => 'ASC']
            );
            $photos[$section->getName()] = $sectionImages;
    
        }
        $globalPage = $this->pageRepo->findOneByName('global');
        
        if($globalPage){

            foreach ($globalPage->getSections() as $key => $section) {
                $sectionImages = $this->photoRepo->findBy(
                    ['section' => $section],
                    ['id' => 'ASC']
                );
                $photos[$section->getName()] = $sectionImages;
        
            }

        }

        return $photos;
    }

    function getImgForms($webPhotos){

        $photos = [];
        $photoIds = [];
    
        $buildImgLinks = "";
        foreach($webPhotos as $wp){
            $photos[] = $wp->getUrl();
            $photoIds[] = $wp->getId().'*'.$wp->getUrl();
            $currentPath = $_SERVER['REQUEST_URI'];
            $thumnail = $wp->getThumbnail();
            $url = $wp->getUrl();
            $id = $wp->getId();
            $buildImgLinks .= '<div style="width:85%;margin:auto;">';
            $buildImgLinks .= '<form name="photo" style="float:left;border:1px solid gray;margin-top:5px;margin-right:5px;padding:5px;" id="imageUploadForm-'.$id.'" enctype="multipart/form-data" action="/upload/website/photo" method="post">';
            $buildImgLinks .= '<input type="file" style="margin:5px;" hidden="hidden" id="ImageBrowse-'.$id.'" name="image" size=""/>';
            // $buildImgLinks .= '<input type="submit" name="upload" value="Upload" />';
            $buildImgLinks .= '<input type="hidden" name="imageId" value="'.$id.'" />';
            $buildImgLinks .= '<input type="hidden" name="toPage" value="'.$currentPath.'" />';
            $buildImgLinks .= '<img width="100" title="' . $url . '" style="border:#000; z-index:1;position: relative; border-width:2px; float:left; width:auto;" height="100px" src="'. $url .'" id="thumbnail-'.$id.'"/>';
            $buildImgLinks .= '</form>';
            $buildImgLinks .= '</div>';
        }

        return [$buildImgLinks, $photos];

    }
    
    function getImages($html){

        if (preg_match_all('/#%(.*?)%#/', $html, $match)){
            return $match;
        }  else {
            return 'no images';
        }
    }

    function replacePlaceholdersWithImages($photosForThisContent, $html){
        $pattern = [];
        $replace = [];
        foreach ($photosForThisContent as $key => $imgP ) {
            $pattern[] = '/#%(.*?)-'.$key.'%#/';
        }
        foreach ($photosForThisContent as $key => $photo) {
            $replace[] = $photo;
        }
        // $pattern = ['/#%(.*?)-1%#/','/#%(.*?)-2%#/','/#%(.*?)-3%#/'];
        // $replace = ["image-1","image-2","image-3"];
        // echo "<pre>";  
        // print_r($pattern);
        // print_r($replace);
        return preg_replace($pattern, $replace, htmlentities($html));   
        // echo "</pre>";

    }
    
        
    /**
     * @Route("/security/select", name="select_reg")
     */
    public function select_reg(): Response
    {
        return $this->render('default/select_reg.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/testing/website/stuff", name="testing_stuff")
     */
    public function test(UserRepository $userRepo): Response
    {
        $url = "https://maps.googleapis.com/maps/api/place/details/json?key=Yourkey&placeid=ChIJoUKQbC0XLxgRg4zYV6RLEgk";
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec ($ch);
        $res        = json_decode($result,true);
        $reviews    = $res['result']['reviews'];
        return $this->render('default/cropper.html.twig', [
            'student' => $reviews,
        ]);
    }

    /**
     * @Route("/testing/website/stuff/again", name="testing_stuff_again")
     */
    public function testagain(UserRepository $userRepo): Response
    {
        $student = $userRepo->findOneById(5);
        return $this->render('default/cropperFromUrl.html.twig', [
            'student' => $student,
        ]);
    }

    /**
     * @Route("/resize/website/photo/{photo_id}", name="resize_uploaded_web_photo")
     */
    public function replaceUploadedPhoto(WebPhotosRepository $photoRepo, $photo_id): Response
    {
        $photo = $photoRepo->findOneById($photo_id);
        return $this->render('default/cropperFromUrl.html.twig', [
            'photo' => $photo,
        ]);
    }

    /**
     * @Route("/crop/resize/and/replace/{url}", name="crop_resize_replace")
     */
    public function resizeAndReplace(WebPhotosRepository $photoRepo, $url): Response
    {
        return $this->render('default/cropAndReplace.html.twig', [
            'url' => $url,
        ]);
    }

    public function checkConfigs()
    {
        // var_dump(count($this->config->findAll()));
        if(count($this->config->findAll()) == 0){
            return $this->redirectToRoute('configuration_new');
        }
        // if(count($this->pageRepo->findAll()) == 0){
        //     return $this->redirectToRoute('page_new');
        // }

        // if(count($this->sectionRepo->findAll()) == 0){
        //     return $this->redirectToRoute('section_new');
        // }

    }

    public function generateTemplate($html, $template, $variables)
    {
        $loader = new \Twig\Loader\ArrayLoader([
            $template => $html,
            'header_global' => $this->sectionRepo->findOneByName('header_global') ? $this->sectionRepo->findOneByName('header_global')->getTypography() : null,
            'footer_global' => $this->sectionRepo->findOneByName('footer_global') ? $this->sectionRepo->findOneByName('footer_global')->getTypography() : null,
            'address_global' => $this->sectionRepo->findOneByName('address_global') ? $this->sectionRepo->findOneByName('address_global')->getTypography() : null,
            'contact-mini-form' => $this->sectionRepo->findOneByName('contact-mini-form') ? $this->sectionRepo->findOneByName('contact-mini-form')->getTypography() : null,
            'pricing_global' => $this->sectionRepo->findOneByName('pricing_global') ? $this->sectionRepo->findOneByName('pricing_global')->getTypography() : null,
            'sidebar_right' => $this->sectionRepo->findOneByName('sidebar_right') ? $this->sectionRepo->findOneByName('sidebar_right')->getTypography() : null,
            'book_lessons' => $this->sectionRepo->findOneByName('book_lessons') ? $this->sectionRepo->findOneByName('book_lessons')->getTypography() : null,
            'start_learning' => $this->sectionRepo->findOneByName('start_learning') ? $this->sectionRepo->findOneByName('start_learning')->getTypography() : null,
            'contactform_global' => $this->sectionRepo->findOneByName('contactform_global') ? $this->sectionRepo->findOneByName('contactform_global')->getTypography() : null,
            'teacherinfo' => $this->sectionRepo->findOneByName('teacherinfo') ? $this->sectionRepo->findOneByName('teacherinfo')->getTypography() : null,
            'courseinfo' => $this->sectionRepo->findOneByName('courseinfo') ? $this->sectionRepo->findOneByName('courseinfo')->getTypography() : null,
            'bloginfo' => $this->sectionRepo->findOneByName('bloginfo') ? $this->sectionRepo->findOneByName('bloginfo')->getTypography() : null,
        ]);        
                
        $twig = new \Twig\Environment($loader);
        $profile = new \Twig\Profiler\Profile();
        $context = new Routing\RequestContext();
        $router = $this->container->get('router');
        $routes = $router->getRouteCollection();

        $generator = new Routing\Generator\UrlGenerator($routes, $context);
        $versionStrategy = new StaticVersionStrategy('v1');
        $defaultPackage = new Package($versionStrategy);
        $namedPackages = [
            'img' => new UrlPackage('http://img.example.com/', $versionStrategy),
            'doc' => new PathPackage('/somewhere/deep/for/documents', $versionStrategy),
        ];

        $packages = new Packages($defaultPackage, $namedPackages);

        // this enables template_from_string function
        $twig->addExtension(new \Twig\Extension\StringLoaderExtension());
        // this enables debug
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        // this enables {{asset('somefile')}}
        $twig->addExtension(new AssetExtension($packages));
        // this enables {{path('someroute')}}
        $twig->addExtension(new RoutingExtension($generator));

        $rendered = $twig->render($template, ['global_vars' => $variables]);

        return $rendered;
        
    }

    // /**
    //  * @Route("/test/for/cheza/music/school", name="test")
    //  */
    // public function testing(PageRepository $pageRepo, SectionRepository $sectionRepo): Response
    // {
    //     // this page
    //     $page = $pageRepo->findOneByName('about');

    //     // html is now empty
    //     $html = "<html><body><h1>Hello from test</h1></body></html>";

    //     // THIS WAS THE PROBLEMATIC PART
    //     $html .= '{{ include("header.html.twig") }}';

    //     $variables = ['this' => 'that'];

    //     // HERE IS THE SOLUTION -> ADD THE NEW TEMPLATE TO THE ARRAY LOADER 
    //     $loader = new \Twig\Loader\ArrayLoader([
    //         'test.html.twig' => '{{ include(template_from_string(html)) }}',
    //         'header.html.twig' =>  "<html><body><h1>Hello from {{name}}</h1></body></html>",        
    //     ]);
        
    //     $twig = new \Twig\Environment($loader);
    //     $profile = new \Twig\Profiler\Profile();
    //     $context = new Routing\RequestContext();
    //     $router = $this->get('router');
    //     $routes = $router->getRouteCollection();

    //     $generator = new Routing\Generator\UrlGenerator($routes, $context);
    //     $versionStrategy = new StaticVersionStrategy('v1');
    //     $defaultPackage = new Package($versionStrategy);
    //     $namedPackages = [
    //         'img' => new UrlPackage('http://img.example.com/', $versionStrategy),
    //         'doc' => new PathPackage('/somewhere/deep/for/documents', $versionStrategy),
    //     ];

    //     $packages = new Packages($defaultPackage, $namedPackages);

    //     // this enables template_from_string function
    //     $twig->addExtension(new \Twig\Extension\StringLoaderExtension());
    //     // this enables debug
    //     $twig->addExtension(new \Twig\Extension\DebugExtension());
    //     // this enables {{asset('somefile')}}
    //     $twig->addExtension(new AssetExtension($packages));
    //     // this enables {{path('someroute')}}
    //     $twig->addExtension(new RoutingExtension($generator));

    //     $response = new Response();
        
    //     $response->setContent($twig->render('test.html.twig', ['html' => $html, 'name' => 'header']));
    //     $response->setStatusCode(Response::HTTP_OK);
        
    //     // sets a HTTP response header
    //     $response->headers->set('Content-Type', 'text/html');
        
    //     // return response
    //     return $response;
        
    // }

    // Define function to test
    function _is_curl_installed() {
        if  (in_array  ('curl', get_loaded_extensions())) {
            return true;
        }
        else {
            return false;
        }
    }

    public function fullNumber($num){
    	$number = "";
        if(strlen($num) == 10){
            $number = preg_replace('/^0/', '+254', $num);
        } elseif(strlen($num) == 13) {
            $number = $num; 
        } else {
            $number = $num;
        }
        return $number;
    }

}
