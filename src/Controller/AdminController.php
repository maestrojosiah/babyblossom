<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'admin-dashboard')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    #[Route('/admin/security/role', name: 'assign_access_roles')]
    public function assign_admin_role(UserRepository $userRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $admins = $userRepo->findBy(
            array('usertype' => 'admin'),
            array('id' => 'DESC')
        );
    
        $normal = $userRepo->findBy(
            array('usertype' => 'normal'),
            array('id' => 'DESC')
        );
    
    
        // $admins = $userRepo->findByUsertype('admin');
        // $students = $userRepo->findByUsertype('student');
        // $teachers = $userRepo->findByUsertype('teacher');
        return $this->render('admin/roles.html.twig', [
            'admins' => $admins,
            'normal' => $normal,
        ]);
        }

    #[Route('/admin/security/now/role/add/new/{usertype}/{user_id}/{roletype}', name: 'add_role')]
    public function assign_new_role(UserRepository $userRepo, $usertype, $user_id, $roletype): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $userRepo->findOneById($user_id);
        $has_role = $user->hasRole($user->getRoles(), $roletype);
        if($has_role){
            $user->setRoles([]);
        } else {
            $roles = [];
            $roles[] = $roletype;
            $user->setRoles([$roletype]);
        }
        
        $userRepo->save($user, true); 

        return $this->redirectToRoute('assign_access_roles');
    }

    /**
     * @Route("/admin/security/now/activate/{user_id}", name="toggle_activate")
     */
    public function toggleStudentActivate(UserRepository $userRepo, $user_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $userRepo->findOneById($user_id);
        $active = $user->getActive();
        if($user->getUsertype() == 'student'){
            $uigActives = $user->getStudentdata()->getUserInstruments();
        } else {
            $uigActives = $user->getTeacherdata()->getUserInstruments();
        }
        
        $activeText = "";
        if($active){
            $user->setActive(false);
            $user->setIsVerified(false);
            foreach ($uigActives as $uigActive ) {
                $uigActive->setActive(false);
                $userRepo->save($uigActive, false); 
            }
            $activeText = "Inactive";
        } else {
            $user->setActive(true);
            $user->setIsVerified(true);
            foreach ($uigActives as $uigActive ) {
                $uigActive->setActive(true);
                $userRepo->save($uigActive, false); 
            }
            $activeText = "Active";
        }
        
        
        $userRepo->save($user, true);
        return $this->redirectToRoute('assign_access_roles');
        // return $this->render('default/test.html.twig', [
        //     'test' => $uigActive,
        // ]);
    }


}
