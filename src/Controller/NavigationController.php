<?php

namespace App\Controller;

use App\Entity\Navigation;
use App\Form\NavigationType;
use App\Repository\NavigationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/navigation')]
class NavigationController extends AbstractController
{
    #[Route('/', name: 'app_navigation_index', methods: ['GET'])]
    public function index(NavigationRepository $navigationRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('navigation/index.html.twig', [
            'navigations' => $navigationRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_navigation_new', methods: ['GET', 'POST'])]
    public function new(Request $request, NavigationRepository $navigationRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $navigation = new Navigation();
        $form = $this->createForm(NavigationType::class, $navigation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $navigationRepository->save($navigation, true);

            return $this->redirectToRoute('app_navigation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('navigation/new.html.twig', [
            'navigation' => $navigation,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_navigation_show', methods: ['GET'])]
    public function show(Navigation $navigation): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('navigation/show.html.twig', [
            'navigation' => $navigation,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_navigation_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Navigation $navigation, NavigationRepository $navigationRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(NavigationType::class, $navigation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $navigationRepository->save($navigation, true);

            return $this->redirectToRoute('app_navigation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('navigation/edit.html.twig', [
            'navigation' => $navigation,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_navigation_delete', methods: ['POST'])]
    public function delete(Request $request, Navigation $navigation, NavigationRepository $navigationRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$navigation->getId(), $request->request->get('_token'))) {
            $navigationRepository->remove($navigation, true);
        }

        return $this->redirectToRoute('app_navigation_index', [], Response::HTTP_SEE_OTHER);
    }
}
