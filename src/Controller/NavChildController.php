<?php

namespace App\Controller;

use App\Entity\NavChild;
use App\Form\NavChildType;
use App\Repository\NavChildRepository;
use App\Repository\NavigationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/nav/child')]
class NavChildController extends AbstractController
{
    #[Route('/', name: 'app_nav_child_index', methods: ['GET'])]
    public function index(NavChildRepository $navChildRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('nav_child/index.html.twig', [
            'nav_children' => $navChildRepository->findAll(),
        ]);
    }

    #[Route('/new/for/{navigation_id}', name: 'app_nav_child_new', methods: ['GET', 'POST'])]
    public function new(Request $request, NavigationRepository $navRepo, NavChildRepository $navChildRepository, $navigation_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $navChild = new NavChild();
        $navigation = $navRepo->findOneById($navigation_id);
        $form = $this->createForm(NavChildType::class, $navChild);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $navChild->setNavigation($navigation);
            $navChildRepository->save($navChild, true);

            return $this->redirectToRoute('app_nav_child_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('nav_child/new.html.twig', [
            'nav_child' => $navChild,
            'form' => $form,
        ]);
    }
    
    #[Route('/{id}', name: 'app_nav_child_show', methods: ['GET'])]
    public function show(NavChild $navChild): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('nav_child/show.html.twig', [
            'nav_child' => $navChild,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_nav_child_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, NavChild $navChild, NavChildRepository $navChildRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(NavChildType::class, $navChild);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $navChildRepository->save($navChild, true);

            return $this->redirectToRoute('app_nav_child_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('nav_child/edit.html.twig', [
            'nav_child' => $navChild,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_nav_child_delete', methods: ['POST'])]
    public function delete(Request $request, NavChild $navChild, NavChildRepository $navChildRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$navChild->getId(), $request->request->get('_token'))) {
            $navChildRepository->remove($navChild, true);
        }

        return $this->redirectToRoute('app_nav_child_index', [], Response::HTTP_SEE_OTHER);
    }
}
