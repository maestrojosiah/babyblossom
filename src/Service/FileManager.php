<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Knp\Snappy\Pdf;
use Twig\Environment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FileManager extends AbstractController
{
    private $mailer;
    private $twig;

    public function __construct(MailerInterface $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;

    }

    public function deleteIfFileExists($file, $entry, $path){

        if(@is_array(getimagesize($file))){
            $image = true;
        } else {
            $image = false;
        }
        if($image == true) {
            //delete the current photo if available
            if($entry != null ) {
                $current_image_path = $this->getParameter("$path")."/".$entry;
                $current_image_thumb_path = $this->getParameter("$path")."/thumbs/".$entry;
                if(file_exists($current_image_path)){ unlink($current_image_path); }
                if(file_exists($current_image_thumb_path)){ unlink($current_image_thumb_path); }
            }
        }

        return true;
    }

}